// DP
#include <cstdio>
#include <climits>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    vector<vector<int>> a(n, vector<int>(m));
    for (int i = 0; i < n; ++i) {
        scanf("%d", &a[i][0]);
        if (i) a[i][0] = INT_MIN >> 1;
        for (int j = 1; j < m; ++j) {
            int u1 = a[i][j - 1], u2;
            scanf("%d", &u2);
            if (i) u2 += a[i - 1][j - 1];
            a[i][j] = max(u1, u2);
        }
    }
    printf("%d\n", a[--n][--m]);
    vector<int> sol;
    for (; n >= 0; --n) {
        while (m && a[n][m] == a[n][m - 1]) --m;
        sol.emplace_back(m--);
    }
    for (int i = sol.size() - 1; i >= 0; --i)
        printf("%d ", sol[i] + 1);
    printf("\n");
    return 0;
}
