// Recursion
#include <cstdio>
#include <utility>
using namespace std;

int main() {
    unsigned a, b;
    int ans = 0;
    scanf("%u%u", &a, &b);
    for (;;)
        if (!a || !b) break;
        else if ((a + b) & 1) {
            ans = -1;
            break;
        } else if (a & 1) {
            if (a > b) swap(a, b);
            b -= a;
            a += a;
            ++ans;
        } else {
            a >>= 1;
            b >>= 1;
        }
    printf("%d\n", ans);
}
