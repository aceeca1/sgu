// Combinatorics
#include <cstdio>
using namespace std;

int main() {
    unsigned n;
    scanf("%u", &n);
    printf("%u\n", (n * (n + 1) >> 1) + 1);
    return 0;
}
