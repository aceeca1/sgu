// Number Theory
#include <cstdio>
#include <cmath>
#include <utility>
using namespace std;

// Snippet: exEuclid
void exEuclid(long long a, long long b, long long& p, long long& u) {
    u = 1;
    p = 0;
    while (b) {
        long long k = a / b;
        swap(a -= k * b, b);
        swap(u -= k * p, p);
    }
    p = a;
}

int main() {
    int x1, x2, p, k;
    scanf("%d%d%d%d", &x1, &x2, &p, &k);
    bool swapped = false;
    if (x1 > x2) {
        swap(x1, x2);
        swapped = true;
    }
    long long u, v;
    exEuclid(x1, x2, u, v);
    if (p % u) {
        printf("NO\n");
        return 0;
    }
    p /= u;
    x1 /= u;
    x2 /= u;
    if (x1 & x2 & 1 && (p ^ k) & 1) {
        printf("NO\n");
        return 0;
    }
    v = v * p % x2;
    u = (p - v * x1) / x2;
    int d = x2;
    if (!(x1 & x2 & 1)) {
        if ((u + v + k) & 1) {
            v = v > 0 ? v - x2 : v + x2;
            u = (p - v * x1) / x2;
        }
        d = x2 + x2;
    }
    int a = abs(u) + abs(v);
    int v1 = v > 0 ? v - d : v + d;
    int u1 = (p - (long long)v1 * x1) / x2;
    int a1 = abs(u1) + abs(v1);
    if (a1 < a) { a = a1; u = u1; v = v1; }
    if (a > k) printf("NO\n");
    else {
        a = (k - a) >> 1;
        int p1, n1, p2, n2;
        if (v > 0) { p1 = v + a; n1 = a; }
        else { p1 = a; n1 = a - v; }
        if (u > 0) { p2 = u; n2 = 0; }
        else { p2 = 0; n2 = -u; }
        if (swapped) { swap(p1, p2); swap(n1, n2); }
        printf("YES\n%d %d %d %d\n", p1, n1, p2, n2);
    }
    return 0;
}
