// Construction + DFS
#include <cstdio>
#include <vector>
using namespace std;

struct Island {
    vector<int> e;
    int v, d;
};

vector<Island> v;
int n;

void revColor(int n1, int n2) {
    v[n1].e[n2] = -v[n1].e[n2];
    v[n2].e[n1] = -v[n2].e[n1];
}

void color3(int no, int p, bool neg) {
    v[no].v = 1;
    for (int i = 0; i < n; ++i)
        if (i != p && v[no].e[i] && v[i].v < 2) {
            if (neg != (v[no].e[i] < 0)) revColor(no, i);
            if (!v[i].v) color3(i, no, !neg);
            neg = !neg;
        }
    v[no].v = 2;
}

void recolor3(int no, int p, bool neg) {
    for (int i = 0; i < n; ++i)
        if (neg == (v[no].e[i] < 0)) return;
    v[no].v = 3;
    for (int i = 0; i < n; ++i)
        if (i != p && v[no].e[i] && v[i].v == 2) {
            revColor(no, i);
            return recolor3(i, no, !neg);
        }
    for (int i = 0; i < n; ++i)
        if (i != p && v[no].e[i]) return revColor(no, i);
}

void color2(int no, int p, bool neg) {
    v[no].v = neg ? 2 : 1;
    for (int i = 0; i < n; ++i)
        if (i != p && v[no].e[i]) {
            if (neg != (v[no].e[i] < 0)) revColor(no, i);
            if (v[i].v) throw v[i].v == (neg ? 1 : 2);
            color2(i, no, !neg);
            neg = !neg;
        }
}

int main() {
    scanf("%d", &n);
    v.resize(n);
    for (int i = 0; i < n; ++i) {
        v[i].e.resize(n);
        for (;;) {
            int a;
            scanf("%d", &a);
            if (!a) break;
            v[i].e[a - 1] = ++v[i].d;
        }
    }
    for (int i = 0; i < n; ++i)
        if (!v[i].v && v[i].d >= 3) {
            color3(i, -1, false);
            recolor3(i, -1, true);
        }
    for (int i = 0; i < n; ++i) if (!v[i].v) {
        try { color2(i, -1, false); }
        catch (bool e) {
            if (!e) {
                printf("No solution\n");
                return 0;
            }
        }
    }
    for (int i = 0; i < n; ++i) {
        vector<int> ans(v[i].d + 1);
        for (int j = 0; j < n; ++j)
            if (v[i].e[j] > 0) ans[v[i].e[j]] = 1;
            else ans[-v[i].e[j]] = 2;
        for (int j = 1; j <= v[i].d; ++j)
            printf("%d ", ans[j]);
        printf("0\n");
    }
}
