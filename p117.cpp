#include <cstdio>
using namespace std;

// Snippet: powM
long long powM(long long a, long long b, long long m) {
    long long ans = 1;
    for (; b; b >>= 1) {
        if (b & 1) ans = ans * a % m;
        a = a * a % m;
    }
    return ans;
}

int main() {
    int n, m, k, ans = 0;
    scanf("%d%d%d", &n, &m, &k);
    while (n--) {
        int a;
        scanf("%d", &a);
        ans += !powM(a, m, k);
    }
    printf("%d\n", ans);
    return 0;
}
