// Geometry + Simulation
#include <cstdio>
#include <cmath>
#include <vector>
#include <valarray>
using namespace std;

typedef valarray<double> Point;

struct Ball {
    Point c;
    double r;
    Ball(): c(3) {}
};

struct Ray {
    Point s, d;
    Ray(): s(3), d(3) {}
};

double getK(const Ray& r, const Ball& b) {
    Point a = b.c - r.s;
    double mid = (a * r.d).sum();
    double d2 = (a * a).sum() - mid * mid;
    double r2 = b.r * b.r;
    if (d2 - r2 > 1e-12) return -1.0;
    if (d2 > r2) d2 = r2;
    double e = sqrt(r2 - d2);
    return mid - e;
}

void getR(Ray& r, const Ball& b, double k) {
    r.s += r.d * k;
    Point a = r.s - b.c;
    a /= sqrt((a * a).sum());
    a *= -(r.d * a).sum() * 2.0;
    r.d += a;
}

int main() {
    int n;
    scanf("%d", &n);
    vector<Ball> b(n);
    for (int i = 0; i < n; ++i)
        scanf("%lf%lf%lf%lf", &b[i].c[0], &b[i].c[1], &b[i].c[2], &b[i].r);
    Ray r;
    scanf("%lf%lf%lf%lf%lf%lf",
        &r.s[0], &r.s[1], &r.s[2], &r.d[0], &r.d[1], &r.d[2]);
    r.d -= r.s;
    r.d /= sqrt((r.d * r.d).sum());
    int i = 0;
    for (; i < 11; ++i) {
        double minK = 1.0 / 0.0;
        int jMin = -1;
        for (int j = 0; j < (int)b.size(); ++j) {
            double k = getK(r, b[j]);
            if (k <= 1e-12) continue;
            if (k < minK) { minK = k; jMin = j; }
        }
        if (minK == 1.0 / 0.0) break;
        if (i < 10) printf("%d ", jMin + 1);
        else printf("etc.");
        getR(r, b[jMin], minK);
    }
    printf("\n");
    return 0;
}
