// Number Theory
#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    int ans = n;
    for (int i = 2; i * i <= n; ++i) if (!(n % i)) {
        while (!(n % i)) n /= i;
        ans = ans / i * (i - 1);
    }
    if (n > 1) ans = ans / n * (n - 1);
    printf("%d\n", ans);
    return 0;
}
