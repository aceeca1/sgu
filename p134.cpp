// DP
#include <cstdio>
#include <climits>
#include <vector>
#include <algorithm>
using namespace std;

struct Node {
    vector<int> ch;
    int size;
};

vector<Node> v;
vector<int> ans;
int ansV = INT_MAX;

// Snippet: removeV
void removeV(vector<int>& a, int v) {
    a.resize(remove(a.begin(), a.end(), v) - a.begin());
}

int calcSize(int no, int p) {
    removeV(v[no].ch, p);
    int ans = 1;
    for (int i: v[no].ch) ans += calcSize(i, no);
    return v[no].size = ans;
}

void calcAns(int no) {
    int cen = v[1].size - v[no].size;
    for (int i: v[no].ch) {
        calcAns(i);
        if (v[i].size > cen) cen = v[i].size;
    }
    if (cen < ansV) {
        ansV = cen;
        ans.clear();
        ans.emplace_back(no);
    } else if (cen == ansV) ans.emplace_back(no);
}

int main() {
    int n;
    scanf("%d", &n);
    v.resize(n + 1);
    for (int i = 1; i < n; ++i) {
        int a1, a2;
        scanf("%d%d", &a1, &a2);
        v[a1].ch.emplace_back(a2);
        v[a2].ch.emplace_back(a1);
    }
    calcSize(1, 0);
    calcAns(1);
    printf("%d %d\n", ansV, (int)ans.size());
    sort(ans.begin(), ans.end());
    for (int i = 0; i < (int)ans.size(); ++i)
        printf("%d ", ans[i]);
    printf("\n");
    return 0;
}
