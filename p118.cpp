// Number Theory
#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n, ans = 8, a = 1;
        scanf("%d", &n);
        while (n--) {
            int b;
            scanf("%d", &b);
            b %= 9;
            a = a * b % 9;
            ans = (ans + a) % 9;
        }
        printf("%d\n", ans % 9 + 1);
    }
    return 0;
}
