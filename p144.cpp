// Probability
#include <cstdio>
using namespace std;

int main() {
    double x, y, z;
    scanf("%lf%lf%lf", &x, &y, &z);
    double a = (y - x) * 60.0;
    double b = (a - z) / a;
    printf("%.7f\n", 1.0 - b * b);
}
