// Recursion
#include <cstdio>
#include <vector>
using namespace std;

void generate(int n, int k, vector<int>& a) {
    if (n == 1) {
        a.emplace_back(k);
        return;
    }
    vector<int> b;
    int n0 = n - k % n;
    generate(n0, k % n, b);
    a.reserve(n);
    for (int i = 0; i < n0; ++i) {
        a.emplace_back(k / n);
        for (int j = 0; j < b[i]; ++j)
            a.emplace_back(k / n + 1);
    }
}

int main() {
    int n, k;
    scanf("%d%d", &n, &k);
    vector<int> a;
    generate(n, k, a);
    for (int i = 0; i < n; ++i) printf("%d ", a[i]);
    printf("\n");
    return 0;
}
