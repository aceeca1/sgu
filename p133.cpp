// Sorting
#include <cstdio>
#include <utility>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    vector<pair<int, int>> a(n);
    for (int i = 0; i < n; ++i)
        scanf("%d%d", &a[i].first, &a[i].second);
    sort(a.begin(), a.end());
    int z = 0;
    for (int i = 1; i < (int)a.size(); ++i)
        if (a[i].second > a[z].second)
            a[++z] = a[i];
    printf("%d\n", (int)a.size() - z - 1);
    return 0;
}
