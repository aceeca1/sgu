// Number Theory
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <utility>
using namespace std;

// Snippet: exEuclid
void exEuclid(long long a, long long b, long long& p, long long& u) {
    u = 1;
    p = 0;
    while (b) {
        long long k = a / b;
        swap(a -= k * b, b);
        swap(u -= k * p, p);
    }
    p = a;
}

int main() {
    int a, b, c, x1, x2, y1, y2;
    scanf("%d%d%d%d%d%d%d", &a, &b, &c, &x1, &x2, &y1, &y2);
    if (!a) { swap(a, b); swap(x1, y1); swap(x2, y2); }
    if (!a) {
        if (c || x1 > x2 || y1 > y2) printf("0\n");
        else printf("%lld\n", (long long)(x2 - x1 + 1) * (y2 - y1 + 1));
        return 0;
    }
    if (!b) {
        if (c % a) printf("0\n");
        else {
            int u = -c / a;
            if (u < x1 || x2 < u || y1 > y2) printf("0\n");
            else printf("%d\n", y2 - y1 + 1);
        }
        return 0;
    }
    if (a < 0) { a = -a; swap(x1 = -x1, x2 = -x2); }
    if (b < 0) { b = -b; swap(y1 = -y1, y2 = -y2); }
    long long p, u;
    exEuclid(a, b, p, u);
    if (c % p) printf("0\n");
    else {
        a /= p;
        b /= p;
        c /= -p;
        long long v = (1 - a * u) / b * c;
        u *= c;
        double lb1 = ceil(double(x1 - u) / b);
        double ub1 = floor(double(x2 - u) / b);
        double lb2 = ceil(double(v - y2) / a);
        double ub2 = floor(double(v - y1) / a);
        double ans = max(min(ub1, ub2) - max(lb1, lb2) + 1.0, 0.0);
        printf("%.0f\n", ans);
    }
    return 0;
}
