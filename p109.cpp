// Construction
#include <cstdio>
using namespace std;

void removeSingle(int n, int m, int a, int i) {
    int n1 = (a + i) * n + (a + m - 1) + 1;
    int n2 = (a + m - 1) * n + (a + i) + 1;
    printf(" %d", n1);
    if (n2 != n1) printf(" %d", n2);
}

void removeDouble(int n, int m, int a, int i) {
    int n1 = a * n + (a + i) + 1;
    int n2 = (a + i) * n + (a + m - 1) + 1;
    int n3 = (a + m - 1) * n + (a + m - 1 - i) + 1;
    int n4 = (a + m - 1 - i) * n + a + 1;
    printf(" %d %d %d %d", n1, n2, n3, n4);
}

int main() {
    int n;
    scanf("%d", &n);
    int m = n, k = n, a = 0;
    k |= 1;
    while (m > 1)
        if (~m & 1) {
            printf("%d", k);
            k += 2;
            for (int i = 1; i < m; i += 2) removeSingle(n, m, a, i);
            printf("\n");
            printf("%d", k);
            k += 2;
            for (int i = 0; i < m; i += 2) removeSingle(n, m, a, i);
            printf("\n");
            --m;
        } else {
            printf("%d", k);
            k += 2;
            for (int i = 2; i < m; i += 2) removeDouble(n, m, a, i);
            printf("\n");
            printf("%d", k);
            k += 2;
            for (int i = 1; i < m; i += 2) removeDouble(n, m, a, i);
            printf("\n");
            m -= 2;
            ++a;
        }
    return 0;
}
