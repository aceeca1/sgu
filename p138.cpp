// Construction
#include <cstdio>
#include <vector>
#include <algorithm>
#include <queue>
using namespace std;

vector<int> d;

struct Node {
    int no;
    bool operator<(const Node& that) const { return d[no] < d[that.no]; }
};

int main() {
    int n, ans = 0;
    scanf("%d", &n);
    d.resize(n);
    vector<int> a;
    vector<Node> b;
    for (int i = 0; i < n; ++i) {
        scanf("%d", &d[i]);
        ans += d[i];
        if (d[i] == 1) a.emplace_back(i);
        else if (d[i] > 1) b.emplace_back(Node{i});
    }
    ans >>= 1;
    if (ans == 1) {
        printf("1\n");
        printf("%d %d\n", a[0] + 1, a[1] + 1);
        return 0;
    } else printf("%d\n", ans);
    if (b.size() >= 2) {
        sort(b.begin(), b.end());
        swap(b[1], b.back());
        --d[b.front().no];
        for (int i = 1; i < (int)b.size() - 1; ++i)
            d[b[i].no] -= 2;
        --d[b.back().no];
    }
    vector<vector<int>> e(n);
    priority_queue<Node> pq;
    for (auto &i: b) if (d[i.no]) pq.emplace(Node{i});
    for (int i: a) {
        int pqH = pq.top().no;
        pq.pop();
        e[pqH].emplace_back(i);
        if (--d[pqH]) pq.emplace(Node{pqH});
    }
    while (!pq.empty()) {
        int pq1 = pq.top().no;
        pq.pop();
        if (pq.empty()) {
            while (d[pq1]) {
                e[pq1].emplace_back(b.back().no);
                b.pop_back();
                e[pq1].emplace_back(b.back().no);
                d[pq1] -= 2;
            }
            break;
        }
        int pq2 = pq.top().no;
        pq.pop();
        e[pq1].emplace_back(pq2);
        if (--d[pq1]) pq.emplace(Node{pq1});
        if (--d[pq2]) pq.emplace(Node{pq2});
    }
    for (int i = 0; i < (int)b.size(); ++i) {
        if (i) printf("%d %d\n", b[i].no + 1, b[i - 1].no + 1);
        for (int j: e[b[i].no])
            printf("%d %d\n", b[i].no + 1, j + 1);
    }
    return 0;
}
