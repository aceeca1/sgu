// Binary Search + Hash
#include <cstdio>
#include <cmath>
#include <vector>
#include <string>
using namespace std;

string a;
int n;

int nonSub(int m) {
    vector<bool> b(1 << m);
    int c = 0, k = (1 << m) - 1;
    for (int i = 0; i < m; ++i) c += c + (a[i] == 'b');
    for (int i = m; ; ++i) {
        b[c] = true;
        if (i == n) break;
        c = (c + c + (a[i] == 'b')) & ((1 << m) - 1);
    }
    while (k >= 0 && b[k]) --k;
    return k;
}

int main() {
    scanf("%d", &n);
    a.resize(n + 1, 0);
    scanf("%s", &a[0]);
    a.pop_back();
    int s = 1, t = ilogb(n) + 1;
    while (s < t) {
        int mid = (s + t) >> 1;
        if (nonSub(mid) == -1) s = mid + 1;
        else t = mid;
    }
    a.clear();
    int k = nonSub(s);
    for (int i = s - 1; i >= 0; --i) a += (k >> i) & 1 ? 'b' : 'a';
    printf("%d\n%s\n", (int)a.size(), a.c_str());
    return 0;
}
