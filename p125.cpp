// Search
#include <cstdio>
#include <vector>
using namespace std;

vector<vector<int>> a, b;
int n;

void put(int k, int x, int y, int filled) {
    if (filled == n * n) throw true;
    if (y == n) { y = 0; ++x; }
    if (x == n) {
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                if (a[i][j] == k) return put(k - 1, 0, 0, filled);
        return;
    }
    if (a[x][y]) return put(k, x, y + 1, filled);
    int t = 0;
    t += x && a[x - 1][y] > k;
    t += y && a[x][y - 1] > k;
    t += x + 1 < n && a[x + 1][y] > k;
    t += y + 1 < n && a[x][y + 1] > k;
    if (t == b[x][y]) {
        a[x][y] = k;
        put(k, x, y + 1, filled + 1);
        a[x][y] = 0;
    }
    put(k, x, y + 1, filled);
}

int main() {
    scanf("%d", &n);
    b.resize(n, vector<int>(n));
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            scanf("%d", &b[i][j]);
    a.resize(n, vector<int>(n));
    try { put(9, 0, 0, 0); }
    catch (bool e) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j)
                printf("%d ", a[i][j]);
            printf("\n");
        }
        return 0;
    }
    printf("NO SOLUTION\n");
    return 0;
}
