// Number Theory + Knapsack
#include <cstdio>
#include <climits>
#include <vector>
using namespace std;

// Snippet: getPrime
void getPrime(int m, vector<int>& p) {
    p.resize(m + 1);
    int u = 0;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
    }
    p[u] = m + 1;
}

int main() {
    int n;
    scanf("%d", &n);
    vector<int> p;
    getPrime(n, p);
    vector<int> a(n + 1, INT_MAX - 1), from(n + 1);
    a[0] = 0;
    int no = 0;
    for (int i = 2; i <= n; i = p[i]) {
        ++no;
        if (p[no] < no) continue;
        for (int j = i; j <= n; ++j) {
            int t = a[j - i] + 1;
            if (t > a[j]) continue;
            a[j] = t;
            from[j] = j - i;
        }
    }
    if (a[n] == INT_MAX - 1) printf("0\n");
    else {
        printf("%d\n", a[n]);
        while (n) {
            printf("%d ", n - from[n]);
            n = from[n];
        }
        printf("\n");
    }
    return 0;
}
