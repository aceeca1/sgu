// Shortest Path + Simulation
#include <cstdio>
#include <climits>
#include <vector>
#include <algorithm>
#include <deque>
using namespace std;

struct Edge {
    int t, c;
    bool operator<(const Edge& that) { return c < that.c; }
};

struct Junction {
    char c;
    int rc, tB, tP, d, p;
    vector<Edge> out;
};

void tick(int tB, int tP, int d, char& c, int& rc) {
    while (d >= rc) {
        d -= rc;
        c ^= 'B' ^ 'P';
        rc = c == 'B' ? tB : tP;
    }
    rc -= d;
}

int waiting(const Junction& s, const Junction& t, int d) {
    if (s.c != t.c && s.rc == t.rc &&
        s.tB == t.tP && s.tP == t.tB) return INT_MAX >> 1;
    char cS = s.c, cT = t.c;
    int rcS = s.rc, rcT = t.rc;
    tick(s.tB, s.tP, d % (s.tB + s.tP), cS, rcS);
    tick(t.tB, t.tP, d % (t.tB + t.tP), cT, rcT);
    int ans = d;
    while (cS != cT) {
        int rc = min(rcS, rcT);
        ans += rc;
        tick(s.tB, s.tP, rc, cS, rcS);
        tick(t.tB, t.tP, rc, cT, rcT);
    }
    return ans;
}

int main() {
    int s, t, n, m;
    scanf("%d%d%d%d", &s, &t, &n, &m);
    vector<Junction> v(n + 1);
    for (int i = 1; i <= n; ++i) {
        scanf(" %c%d%d%d", &v[i].c, &v[i].rc, &v[i].tB, &v[i].tP);
        v[i].d = INT_MAX;
    }
    for (int i = 0; i < m; ++i) {
        int s, t, c;
        scanf("%d%d%d", &s, &t, &c);
        v[s].out.emplace_back(Edge{t, c});
        v[t].out.emplace_back(Edge{s, c});
    }
    for (int i = 1; i <= n; ++i) sort(v[i].out.begin(), v[i].out.end());
    v[s].d = 0;
    deque<int> dq;
    dq.emplace_back(s);
    while (!dq.empty()) {
        int dqH = dq.front();
        dq.pop_front();
        for (Edge& i: v[dqH].out) {
            int t = waiting(v[dqH], v[i.t], v[dqH].d) + i.c;
            if (t < v[i.t].d) {
                v[i.t].d = t;
                v[i.t].p = dqH;
                if (dq.empty() || v[i.t].d < v[dq.front()].d)
                    dq.emplace_front(i.t);
                else dq.emplace_back(i.t);
            }
        }
    }
    if (v[t].d >= INT_MAX >> 1) printf("0\n");
    else {
        printf("%d\n", v[t].d);
        vector<int> sol{t};
        while (t != s) {
            t = v[t].p;
            sol.emplace_back(t);
        }
        for (int i = sol.size() - 1; i >= 0; --i)
            printf("%d ", sol[i]);
        printf("\n");
    }
    return 0;
}
