// Construction
#include <cstdio>
#include <vector>
using namespace std;

struct Person {
    vector<bool> e;
    int next;
};

vector<Person> v;
int n, head = 0, tail = 0, len = 1;

bool extendHead() {
    for (int i = 0; i < n; ++i)
        if (v[i].next == -2 && v[head].e[i]) {
            v[i].next = head;
            head = i;
            return true;
        }
    return false;
}

bool extendTail() {
    for (int i = 0; i < n; ++i)
        if (v[i].next == -2 && v[tail].e[i]) {
            v[tail].next = i;
            v[i].next = -1;
            tail = i;
            return true;
        }
    return false;
}

int reverse(int no) {
    int h = -1;
    while (no != -1) {
        int n = v[no].next;
        v[no].next = h;
        h = no;
        no = n;
    }
    return h;
}

void formCycle() {
    for (int i = 0; i < n; ++i)
        if (v[i].next >= 0 && v[tail].e[i] && v[head].e[v[i].next]) {
            tail = v[i].next;
            v[i].next = reverse(v[i].next);
            v[tail].next = head;
            return;
        }
}

void addPerson() {
    int no = 0;
    while (v[no].next != -2) ++no;
    for (int i = 0; i < n; ++i) if (v[i].next != -2 && v[no].e[i]) {
        head = v[i].next;
        v[i].next = no;
        v[no].next = -1;
        tail = no;
        return;
    }
}

int main() {
    scanf("%d", &n);
    v.resize(n);
    for (int i = 0; i < n; ++i) {
        v[i].e.resize(n);
        for (;;) {
            int a;
            char c = '\n';
            scanf("%d%c", &a, &c);
            v[i].e[a - 1] = 1;
            if (c == '\n') break;
        }
        v[i].next = -2;
    }
    v[0].next = -1;
    while (len < n) {
        while (extendHead()) ++len;
        if (len == n) break;
        while (extendTail()) ++len;
        if (len == n) break;
        formCycle();
        addPerson();
        ++len;
    }
    formCycle();
    putchar('1');
    int ans = 0;
    for (int i = v[0].next; i; i = v[i].next) {
        printf(" %d", i + 1);
        ++ans;
    }
    printf(" 1\n");
    return 0;
}
