// Geometry
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

struct City {
    int x, p;
    bool operator<(const City& that) const { return x < that.x; }
};

int main() {
    int n;
    scanf("%d", &n);
    vector<City> v(n);
    for (int i = 0; i < n; ++i)
        scanf("%d%d", &v[i].x, &v[i].p);
    sort(v.begin(), v.end());
    int s = 0, t = n - 1;
    while (s < t)
        if (v[s].p < v[t].p) v[t].p -= v[s++].p;
        else if (v[s].p > v[t].p) v[s].p -= v[t--].p;
        else ++s, --t;
    printf("%.5f\n", (v[s].x + v[t].x) * 0.5);
    return 0;
}
