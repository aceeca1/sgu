// DP
#include <cstdio>
#include <climits>
#include <vector>
using namespace std;

struct Node {
    vector<int> ch;
    int a;
};

vector<Node> v;
int ans = INT_MIN;

int visit(int no, int p) {
    for (int i: v[no].ch) if (i != p) {
        int t = visit(i, no);
        if (t > 0) v[no].a += t;
    }
    if (v[no].a > ans) ans = v[no].a;
    return v[no].a;
}

int main() {
    int n;
    scanf("%d", &n);
    v.resize(n + 1);
    for (int i = 1; i <= n; ++i) scanf("%d", &v[i].a);
    while (--n) {
        int n1, n2;
        scanf("%d%d", &n1, &n2);
        v[n1].ch.emplace_back(n2);
        v[n2].ch.emplace_back(n1);
    }
    visit(1, 0);
    printf("%d\n", ans);
    return 0;
}
