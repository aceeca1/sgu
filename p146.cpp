// Simulation
#include <cstdio>
#include <cmath>
using namespace std;

int main() {
    double lD;
    long long l, c = 0;
    int n;
    scanf("%lf%d", &lD, &n);
    l = llround(lD * 10000);
    while (n--) {
        long long a, b;
        scanf("%lld%lld", &a, &b);
        c += a * b * 10000 % l;
    }
    c %= l;
    long long c1 = l - c;
    if (c1 < c) c = c1;
    printf("%lld.%04lld\n", c / 10000, c % 10000);
    return 0;
}
