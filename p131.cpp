// DP
#include <cstdio>
#include <vector>
using namespace std;

vector<vector<short>> e;
int m, n, from;

void put(int no, int v) {
    if (no == (1 << m) - 1) {
        e[from].emplace_back(v);
        return;
    }
    int lb = ~no;
    lb &= -lb;
    int lb1 = lb << 1, lbm = lb >> 1;
    if (lb1 >> m) lb1 = 0;
    if (~v & lb) {
        put(no + lb, v + lb);
        if (~v & lbm) put(no + lb, v + lb + lbm);
        if (~v & lb1) put(no + lb, v + lb + lb1);
    }
    if (~no & lb1) {
        put(no + lb + lb1, v);
        if (~v & lb)  put(no + lb + lb1, v + lb);
        if (~v & lb1) put(no + lb + lb1, v + lb1);
    }
}

int main() {
    scanf("%d%d", &m, &n);
    e.resize(1 << m);
    for (int i = 0; i < 1 << m; ++i) {
        from = i;
        put(i, 0);
    }
    vector<long long> v0(1 << m), v1(1 << m);
    v1[0] = 1;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < 1 << m; ++j) {
            long long ans = 0;
            for (int k: e[j]) ans += v1[k];
            v0[j] = ans;
        }
        swap(v0, v1);
    }
    printf("%lld\n", v1[0]);
    return 0;
}
