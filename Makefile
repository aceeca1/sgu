objects := $(patsubst %.cpp, %, $(wildcard *.cpp))

all: $(objects)

$(objects): %: %.cpp
	g++ -std=c++11 -Wall -Wextra -Werror -g -Og -o $@ $<

clean:
	rm -rf $(objects)
