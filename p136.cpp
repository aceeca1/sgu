// Algebra
#include <cstdio>
#include <complex>
#include <vector>
using namespace std;

typedef complex<double> Point;

int main() {
    int n;
    scanf("%d", &n);
    vector<Point> a(n);
    for (int i = 0; i < n; ++i) {
        double x, y;
        scanf("%lf%lf", &x, &y);
        a[i] = Point(x, y);
    }
    Point p;
    if (n & 1) {
        for (int i = 0; i < n; ++i) p += a[i];
        for (int i = 1; i < n; i += 2) p -= a[i] + a[i];
    } else {
        p = (a[0] + a[1]) * 0.5;
        Point q = p;
        for (int i = 0; i < n; ++i) q = a[i] + a[i] - q;
        if (norm(p - q) > 1e-12) {
            printf("NO\n");
            return 0;
        }
    }
    printf("YES\n");
    for (int i = 0; i < n; ++i) {
        printf("%.3f %.3f\n", p.real(), p.imag());
        p = a[i] + a[i] - p;
    }
    return 0;
}
