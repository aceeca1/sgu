// DP
#include <cstdio>
#include <vector>
using namespace std;

int m, n;

struct State {
    int k, k1;
    State(): k(0), k1(1) {}
    State(int k_): k(k_), k1(-1) {}
    operator int() const { return k; }

    State& operator>>(int& a) {
        int t;
        if (k1 == -1) {
            k1 = k & 7;
            k >>= 3;
            t = k1 % 3;
            k1 /= 3;
        } else {
            t = k1;
            k1 = -1;
        }
        a = t * 3 + a / 3;
        return *this;
    }

    State& operator<<(int a) {
        k += a * k1;
        if (k1 % 3) k1 *= 3;
        else k1 = (k1 / 3) << 3;
        return *this;
    }

    static int num() {
        return ((n & 1) ? 3 : 1) << ((n >> 1) * 3);
    }
};

vector<int> a0, a1;
char s[11];

void put(int k, State no, int c, State&& v, int used) {
    if (k == n) {
        int &a0v = a0[v];
        if (used < a0v) a0v = used;
        return;
    }
    if (!k) no >> c;
    no >> c;
    if (c == 4 || c == 5 || c == 7 || c == 8)
        put(k + 1, no, 0, move(State(v) << int(s[k] == '.')), used + 1);
    if (c % 3 && s[k] == '.')
        put(k + 1, no, c, move(State(v) << 0), used + 1);
    if (c % 3 < 2) {
        int v0 = s[k] == '.' ? c % 3 + 1 : 0;
        put(k + 1, no, c == 4 ? 6 : c, move(v << v0), used);
    }
}

int main() {
    scanf("%d%d", &m, &n);
    int num = State::num();
    a0.resize(num);
    a1.resize(num, 0x3fffffff);
    scanf("%s", s);
    State s0;
    for (int i = 0; s[i]; ++i) {s0 = s0 << int(s[i] == '.');}
    a1[s0] = 0;
    for (int i = 1; i <= m; ++i) {
        if (i < m) scanf("%s", s);
        else for (int i = 0; s[i]; ++i) s[i] = '*';
        for (int j = 0; j < num; ++j) a0[j] = 0x3fffffff;
        for (int j = 0; j < num; ++j) {
            if (a1[j] >= 0x3fffffff) continue;
            put(0, State(j), 0, State(), a1[j]);
        }
        swap(a0, a1);
    }
    printf("%d\n", a1[0]);
    return 0;
}
