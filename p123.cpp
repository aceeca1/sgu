// Brute Force
#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    int a0, a1 = 1, a2 = 1, ans = 0;
    for (int i = 0; i < n; ++i) {
        ans += a2;
        a0 = a1 + a2;
        a2 = a1;
        a1 = a0;
    }
    printf("%d\n", ans);
    return 0;
}
