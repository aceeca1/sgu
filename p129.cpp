// Computational Geometry
#include <cstdio>
#include <cmath>
#include <complex>
#include <vector>
#include <algorithm>
using namespace std;

double D_NAN = 0.0 / 0.0;

// Snippet: CG2D
namespace CG2D {
    typedef complex<double> Point;

    double cross(const Point& p1, const Point& p2) {
        return p1.real() * p2.imag() - p1.imag() * p2.real();
    }

    double dot(const Point& p1, const Point& p2) {
        return p1.real() * p2.real() + p1.imag() * p2.imag();
    }

    struct Segment {
        Point c1, c2;
    };

    void intersectPoint(const Segment& s1, const Segment& s2, Point& p) {
        Point e = s1.c2 - s1.c1;
        double a1 = cross(e, s2.c1 - s1.c1);
        double a2 = cross(e, s2.c2 - s1.c1);
        if (a1 * a2 > -1e-12) return;
        e = (s2.c1 * a2 - s2.c2 * a1) / (a2 - a1);
        if (dot(s1.c1 - e, s1.c2 - e) < -1e-12) p = e;
    }

    bool isInside(const Point& p, const vector<Point>& a) {
        for (int i = 0; i < (int)a.size() - 1; ++i)
            if (cross(a[i] - p, a[i + 1] - p) < 1e-12) return false;
        return true;
    }

    bool onSeg(const Point& p, const Segment& s) {
        Point v1 = s.c1 - p, v2 = s.c2 - p;
        double c = cross(v1, v2);
        return -1e-12 < c && c < 1e-12 && dot(v1, v2) < -1e-12;
    }
}

using namespace CG2D;

double innerS(const Segment& s, const vector<Point>& a) {
    return isInside((s.c1 + s.c2) * 0.5, a) ? abs(s.c1 - s.c2) : 0.0;
}

double innerLen(const Segment& s, const vector<Point>& a) {
    vector<Point> inP;
    for (int i = 0; i < (int)a.size() - 1; ++i) {
        Point t(D_NAN, D_NAN);
        intersectPoint(s, Segment{a[i], a[i + 1]}, t);
        if (!isnan(t.real())) inP.emplace_back(move(t));
        if (onSeg(a[i], s)) inP.emplace_back(a[i]);
    }
    switch (inP.size()) {
        case 0: return innerS(s, a);
        case 1: {
            double len1 = innerS(Segment{inP[0], s.c1}, a);
            double len2 = innerS(Segment{inP[0], s.c2}, a);
            return len1 + len2;
        }
        case 2: return innerS(Segment{inP[0], inP[1]}, a);
    }
    return 0.0;
}

int main() {
    int n, m;
    scanf("%d", &n);
    vector<Point> a(n);
    Point c;
    for (int i = 0; i < n; ++i) {
        int x, y;
        scanf("%d%d", &x, &y);
        a[i] = Point(x, y);
        c += a[i];
    }
    c /= n;
    sort(a.begin(), a.end(), [&](const Point& c1, const Point& c2) {
        return arg(c1 - c) < arg(c2 - c);
    });
    a.emplace_back(a[0]);
    scanf("%d", &m);
    while (m--) {
        double x1, y1, x2, y2;
        scanf("%lf%lf%lf%lf", &x1, &y1, &x2, &y2);
        Segment s{{x1, y1}, {x2, y2}};
        printf("%.2f\n", innerLen(s, a));
    }
    return 0;
}
