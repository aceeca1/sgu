// Combinatorics
#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    long long ans = 1;
    for (int i = 1; i <= n; ++i)
        ans = ans * (n + n - i + 1) / i;
    printf("%lld %d\n", ans / (n + 1), n + 1);
    return 0;
}
