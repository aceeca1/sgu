// Number Theory
#include <cstdio>
using namespace std;

int main() {
    unsigned a[3]{0, 0, 1}, n;
    scanf("%u", &n);
    printf("%u\n", ((n / 3) << 1) + a[n % 3]);
    return 0;
}
