// Brute Force
#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

struct Query {
    int no, c;
    bool operator<(const Query& that) const { return c < that.c; }
};

int main() {
    int n, k;
    scanf("%d%d", &n, &k);
    vector<bool> a(n + 1);
    for (int i = 1; i <= n; ++i) {
        int u = i;
        for (int j = i; j; j /= 10) u += j % 10;
        if (u <= n) a[u] = true;
    }
    vector<Query> v(k);
    for (int i = 0; i < k; ++i) {
        scanf("%d", &v[i].c);
        v[i].no = i;
    }
    sort(v.begin(), v.end());
    int z = 0, p = 0;
    vector<int> ans(k);
    for (int i = 1; i <= n; ++i) if (!a[i]) {
        ++z;
        while (p < k && z == v[p].c) ans[v[p++].no] = i;
    }
    printf("%d\n", z);
    for (int i = 0; i < k; ++i)
        printf("%d ", ans[i]);
    printf("\n");
    return 0;
}
