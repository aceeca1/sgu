// Number Theory
#include <cstdio>
#include <cmath>
#include <vector>
using namespace std;

// Snippet: getPrime
void getPrime(int m, vector<int>& p) {
    p.resize(m + 1);
    int u = 0;
    for (int i = 2; i <= m; ++i) {
        int v = p[i];
        if (!v) u = p[u] = v = i;
        for (int w = 2; i * w <= m; w = p[w]) {
            p[i * w] = w;
            if (w >= v) break;
        }
    }
    p[u] = m + 1;
}

bool isPrime(const vector<int>& p, int k) {
    if (k < (int)p.size()) return p[k] > k;
    for (int i = 2; i * i <= k; i = p[i])
        if (!(k % i)) return false;
    return true;
}

int main() {
    int n, m = 0;
    scanf("%d", &n);
    vector<int> a(n);
    for (int i = 0; i < n; ++i) {
        scanf("%d", &a[i]);
        if (a[i] > m) m = a[i];
    }
    m = sqrt(m);
    vector<int> p;
    getPrime(m, p);
    for (int n: a) {
        for (int i = 2; i * i <= n; i = p[i]) {
            if (n % i) continue;
            int k = n / i;
            n = !isPrime(p, k);
            break;
        }
        printf("%s\n", n ? "No" : "Yes");
    }
    return 0;
}
