// High Precision
#include <cstdio>
#include <vector>
#include <string>
using namespace std;

// Snippet: BigInt
struct BigInt {
    static constexpr auto M = 1000000000;
    vector<unsigned> v;

    BigInt(unsigned n = 0): v({n}) {}

    unsigned scanU(const char* s, int z) {
        unsigned ans = 0;
        for (int i = 0; i < z; ++i) ans = ans * 10 + (s[i] - '0');
        return ans;
    }

    BigInt(const char* s, int z) {
        for (int i = z; i > 0; i -= 9) {
            int p = i - 9;
            if (p < 0) p = 0;
            v.emplace_back(scanU(s + p, i - p));
        }
    }

    operator string() const {
        auto ans = to_string(v.back());
        char s[11];
        for (int i = v.size() - 2; i >= 0; --i) {
            sprintf(s, "%09u", v[i]);
            ans += s;
        }
        return ans;
    }

    int cmp(const BigInt& that) const {
        if (v.size() > that.v.size()) return 1;
        if (v.size() < that.v.size()) return -1;
        for (int i = v.size() - 1; i >= 0; --i) {
            if (v[i] > that.v[i]) return 1;
            if (v[i] < that.v[i]) return -1;
        }
        return 0;
    }

    BigInt& operator+=(const BigInt& that) {
        if (that.v.size() > v.size()) v.resize(that.v.size());
        unsigned carry = 0;
        for (int i = 0; i < (int)v.size(); ++i) {
            carry += v[i];
            if (i < (int)that.v.size()) carry += that.v[i];
            v[i] = carry % M;
            carry /= M;
        }
        if (carry) v.emplace_back(carry);
        return *this;
    }

    BigInt& operator-=(const BigInt& that) {
        int carry = 0;
        for (int i = 0; i < (int)v.size(); ++i) {
            carry += v[i];
            if (i < (int)that.v.size()) carry -= that.v[i];
            v[i] = (carry + M) % M;
            carry -= v[i];
            carry /= M;
        }
        while (v.size() > 1 && !v.back()) v.pop_back();
        return *this;
    }

    BigInt& operator*=(unsigned that) {
        long long carry = 0;
        for (int i = 0; i < (int)v.size(); ++i) {
            carry += (long long)v[i] * that;
            v[i] = carry % M;
            carry /= M;
        }
        if (carry) v.emplace_back(carry);
        return *this;
    }

    BigInt& operator*=(const BigInt& that) {
        if (v.size() == 1 && !v[0]) return *this;
        if (that.v.size() == 1 && !that.v[0]) return *this = that;
        vector<unsigned> w(v.size() + that.v.size());
        for (int i = 0; i < (int)v.size(); ++i)
            for (int j = 0; j < (int)that.v.size(); ++j) {
                auto t = (unsigned long long)v[i] * that.v[j];
                for (int k = i + j; t; ++k) {
                    t += w[k];
                    w[k] = t % M;
                    t /= M;
                }
            }
        if (!w.back()) w.pop_back();
        v = move(w);
        return *this;
    }

    BigInt& operator/=(unsigned that) {
        unsigned long long carry = 0;
        for (int i = v.size() - 1; i >= 0; --i) {
            carry = carry * M + v[i];
            v[i] = carry / that;
            carry %= that;
        }
        while (v.size() > 1 && !v.back()) v.pop_back();
        return *this;
    }

    BigInt sqrt() const {
        if (v.size() == 1 && v[0] == 1) return *this;
        BigInt s(1), t(*this);
        int z = (t.v.size() - 1) >> 1;
        for (int i = z; i < (int)t.v.size(); ++i)
            t.v[i - z] = t.v[i];
        t.v.resize(t.v.size() - z);
        while (s.cmp(t) < 0) {
            BigInt mid(s);
            mid += t;
            mid /= 2;
            BigInt k(mid);
            k *= mid;
            if (cmp(k) >= 0) s = mid += 1;
            else t = mid;
        }
        return s -= 1;
    }
};

int main() {
    int a, b;
    scanf("%d%d", &a, &b);
    BigInt t1(1), t2(1);
    for (int i = 0; i < b; ++i) t1 *= a;
    for (int i = 0; i < a; ++i) t2 *= b;
    if (t1.cmp(t2) >= 0) {
        t1 -= t2;
        printf("%s\n", string(t1).c_str());
    } else {
        t2 -= t1;
        printf("-%s\n", string(t2).c_str());
    }
    return 0;
}
