// Number Theory
// https://en.wikipedia.org/wiki/Quadratic_residue
#include <cstdio>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    if (n > 9) {
        printf("72");
        for (int i = n - 10; i; --i) putchar('0');
        printf("\n");
    } else if (n == 9) printf("8\n");
    else if (n == 8) printf("0\n");
    else {
        int ans = 0;
        int i = 1;
        while (n--) i *= 10;
        for (--i; i; --i)
            ans += ((long long)i * i % 1000000000 == 987654321);
        printf("%d\n", ans);
    }
    return 0;
}
