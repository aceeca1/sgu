// Number Theory
#include <cstdio>
#include <utility>
#include <vector>
using namespace std;

// Snippet: exEuclid
void exEuclid(long long a, long long b, long long& p, long long& u) {
    u = 1;
    p = 0;
    while (b) {
        long long k = a / b;
        swap(a -= k * b, b);
        swap(u -= k * p, p);
    }
    p = a;
}

int main() {
    int n, p, b;
    scanf("%d%d%d", &n, &p, &b);
    vector<int> a(n + 1), c(n + 1), d(n + 1);
    for (int i = 0; i < n; ++i) {
        scanf("%d", &a[i]);
        a[i] %= p;
    }
    c[n] = a[n] = p;
    for (int i = n - 1; i >= 0; --i) {
        long long u, v;
        exEuclid(c[i + 1], a[i], u, v);
        c[i] = u;
        d[i] = v;
    }
    if (!c[0])
        if (!b) {
            printf("YES\n");
            for (int i = 0; i < n; ++i) printf("0 ");
            printf("\n");
        } else printf("NO\n");
    else if (b % c[0]) printf("NO\n");
    else {
        printf("YES\n");
        b /= c[0];
        for (int i = 0; i < n; ++i) {
            int ans = 0;
            if (a[i]) {
                long long t = c[i] - (long long)d[i] * c[i + 1];
                ans = t / a[i] * b % p;
                if (ans < 0) ans += p;
            }
            printf("%d ", ans);
            b = b * (long long)d[i] % p;
        }
        printf("\n");
    }
    return 0;
}
