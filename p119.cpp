// Number Theory
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int n, a0, b0;
    scanf("%d%d%d", &n, &a0, &b0);
    a0 %= n;
    b0 %= n;
    vector<pair<int, int>> sol;
    sol.emplace_back(a0, b0);
    int a = a0, b = b0;
    for (;;) {
        a = (a + a0) % n;
        b = (b + b0) % n;
        if (a == sol[0].first && b == sol[0].second) break;
        sol.emplace_back(a, b);
    }
    sort(sol.begin(), sol.end());
    printf("%d\n", (int)sol.size());
    for (auto& i: sol)
        printf("%d %d\n", i.first, i.second);
    return 0;
}
