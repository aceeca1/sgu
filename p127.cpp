// Brute Force
#include <cstdio>
using namespace std;

int main() {
    int k, n, a[10]{}, ans = 0;
    scanf("%d%d", &k, &n);
    for (int i = 0; i < n; ++i) {
        char c;
        scanf(" %c%*s", &c);
        ++a[c - '0'];
    }
    for (int i = 0; i < 10; ++i)
        ans += (a[i] + k - 1) / k;
    printf("%d\n", ans + 2);
}
