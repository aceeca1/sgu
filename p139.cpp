// Combinatorics
#include <cstdio>
using namespace std;

int main() {
    int a[16], zero;
    for (int i = 0; i < 16; ++i) {
        scanf("%d", &a[i]);
        if (!a[i]) zero = i;
    }
    zero = (zero >> 2) + (zero & 3);
    for (int i = 0; i < 16; ++i)
        for (int j = i + 1; j < 16; ++j)
            if (a[i] > a[j]) ++zero;
    printf("%s\n", zero & 1 ? "YES" : "NO");
    return 0;
}
