// Simulation
#include <cstdio>
using namespace std;

constexpr int DAYS[12] = {
    31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    if (m > 12 || n > DAYS[m - 1]) {
        printf("Impossible\n");
        return 0;
    }
    for (int i = 0; i < m - 1; ++i) n += DAYS[i];
    printf("%d\n", (n + 6) % 7 + 1);
}
