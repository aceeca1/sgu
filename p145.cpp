// A* Search
// Not accepted due to system bug. Watch out!
#include <cstdio>
#include <climits>
#include <vector>
#include <deque>
#include <queue>
using namespace std;

struct Edge {
    int t, c;
};

struct Vertex {
    vector<Edge> out;
    vector<int> d;
};

vector<Vertex> v;

struct Node;
vector<Node> vn;

struct Node {
    int no, from, c, h;
    bool operator<(const Node& that) const { return h > that.h; }
    Node(int no_, int from_, int c_): no(no_), from(from_), c(c_) {
        h = 0;
        for (int i = from; i != -1; i = vn[i].from)
            if (v[no].d[vn[i].no] > h) h = v[no].d[vn[i].no];
        h += c;
    }

    static bool check(int curr, int next) {
        for (int i = curr; i != -1; i = vn[i].from)
            if (vn[i].no == next) return false;
        return true;
    }
};

int main() {
    int n, m, k, s, t;
    scanf("%d%d%d", &n, &m, &k);
    v.resize(n);
    while (m--) {
        int s, t, c;
        scanf("%d%d%d", &s, &t, &c);
        v[s - 1].out.emplace_back(Edge{t - 1, c});
        v[t - 1].out.emplace_back(Edge{s - 1, c});
    }
    scanf("%d%d", &s, &t);
    --s, --t;
    for (int i = 0; i < n; ++i) v[i].d.resize(n, INT_MAX);
    for (int i = 0; i < n; ++i) {
        v[t].d[i] = 0;
        deque<int> dq;
        dq.emplace_back(t);
        while (!dq.empty()) {
            int dqH = dq.front();
            dq.pop_front();
            for (auto &j: v[dqH].out) if (j.t != i) {
                int u = v[dqH].d[i] + j.c;
                if (u < v[j.t].d[i]) {
                    v[j.t].d[i] = u;
                    if (dq.empty() || v[j.t].d[i] < v[dq.front()].d[i])
                        dq.emplace_front(j.t);
                    else dq.emplace_back(j.t);
                }
            }
        }
    }
    priority_queue<Node> pq;
    pq.emplace(s, -1, 0);
    while (!pq.empty()) {
        vn.emplace_back(pq.top());
        pq.pop();
        if (vn.back().no == t) {
            if (!--k) break;
            continue;
        }
        for (auto &i: v[vn.back().no].out) {
            if (!Node::check(vn.size() - 1, i.t)) continue;
            pq.emplace(i.t, vn.size() - 1, vn.back().c + i.c);
        }
    }
    vector<int> ans;
    for (int i = vn.size() - 1; i != -1; i = vn[i].from)
        ans.emplace_back(vn[i].no);
    printf("%d %d\n", vn.back().c, (int)ans.size());
    for (int i = ans.size() - 1; i >= 0; --i)
        printf("%d ", ans[i] + 1);
    printf("\n");
    return 0;
}
