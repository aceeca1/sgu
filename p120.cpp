// Complex
#include <cstdio>
#include <cmath>
#include <complex>
#include <vector>
using namespace std;

constexpr double PI = 3.14159265358979323846;

int main() {
    int n, n1, n2;
    scanf("%d%d%d", &n, &n1, &n2);
    double x1, y1, x2, y2;
    scanf("%lf%lf%lf%lf", &x1, &y1, &x2, &y2);
    complex<double> c1(x1, y1), c2(x2, y2);
    vector<complex<double>> w(n + 1);
    for (int i = 1; i <= n; ++i) {
        double a = PI * 2.0 / n * i;
        w[i] = complex<double>(cos(a), -sin(a));
    }
    auto q = (c2 - c1) / (w[n2] - w[n1]);
    auto p = c1 - w[n1] * q;
    for (int i = 1; i <= n; ++i) {
        auto k = p + w[i] * q;
        printf("%.6f %.6f\n", k.real() + 1e-12, k.imag() + 1e-12);
    }
}
