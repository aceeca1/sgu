// Construction + Sweep Line
#include <cstdio>
#include <vector>
#include <utility>
#include <algorithm>
#include <unordered_map>
#include <set>
using namespace std;

struct Point {
    int x, y, eX, eY;
};

vector<Point> v;
int n;

void connectX() {
    unordered_map<int, vector<int>> a;
    for (int i = 0; i < n; ++i)
        a[v[i].x].emplace_back(i);
    for (auto &ai: a) {
        auto &ais = ai.second;
        if (ais.size() & 1) throw false;
        sort(ais.begin(), ais.end(), [](int i1, int i2) {
            return v[i1].y < v[i2].y;
        });
        for (int i = 0; i < (int)ais.size(); i += 2) {
            v[ais[i]].eX = ais[i + 1];
            v[ais[i + 1]].eX = ais[i];
        }
    }
}

int tour() {
    int no = 0, ans = 0;
    bool goX = false;
    while (n--) {
        int p = goX ? v[no].eX : v[no].eY;
        if (p == -1) throw false;
        int t = goX ? v[p].y - v[no].y : v[p].x - v[no].x;
        if (t < 0) t = -t;
        ans += t;
        goX = !goX;
        v[no].eX = v[no].eY = -1;
        no = p;
    }
    if (no != 0) for (;;);
    return ans;
}

struct Op {
    int type, x, y1, y2;
    bool operator<(const Op& that) const {
        return x < that.x || (x == that.x && type < that.type);
    }
};

void checkIntersect() {
    vector<Op> op;
    int no = 0;
    bool goX = false;
    for (int i = 0; i < n; ++i) if (goX) {
        int y1 = v[v[no].eX].y;
        int y2 = v[no].y;
        if (y1 > y2) swap(y1, y2);
        op.emplace_back(Op{1, v[no].x, y1, y2});
        no = v[no].eX;
        goX = !goX;
    } else {
        int x1 = v[v[no].eY].x;
        int x2 = v[no].x;
        if (x1 > x2) swap(x1, x2);
        op.emplace_back(Op{0, x1, v[no].y, 0});
        op.emplace_back(Op{2, x2, v[no].y, 0});
        no = v[no].eY;
        goX = !goX;
    }
    sort(op.begin(), op.end());
    set<int> a;
    for (Op& i: op) switch (i.type) {
        case 0: a.emplace(i.y1); break;
        case 1: {
            auto p = a.upper_bound(i.y1);
            if (p != a.end() && *p < i.y2) throw false;
            break;
        }
        case 2: a.erase(i.y1);
    }
}

int main() {
    scanf("%d", &n);
    v.resize(n);
    for (int i = 0; i < n; ++i)
        scanf("%d%d", &v[i].x, &v[i].y);
    try {
        connectX();
        for (int i = 0; i < n; ++i) {
            swap(v[i].x, v[i].y);
            swap(v[i].eX, v[i].eY);
        }
        connectX();
        checkIntersect();
        int len = tour();
        printf("%d\n", len);
    } catch (bool e) {
        printf("0\n");
    }
    return 0;
}
