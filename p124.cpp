// Computational Geometry
#include <cstdio>
#include <vector>
using namespace std;

struct Segment {
    int x1, y1, x2, y2;
};

int main() {
    int n;
    scanf("%d", &n);
    vector<Segment> a(n);
    for (int i = 0; i < n; ++i) {
        scanf("%d%d%d%d", &a[i].x1, &a[i].y1, &a[i].x2, &a[i].y2);
        if (a[i].x1 > a[i].x2) swap(a[i].x1, a[i].x2);
        if (a[i].y1 > a[i].y2) swap(a[i].y1, a[i].y2);
    }
    int x, y;
    bool in = false;
    scanf("%d%d", &x, &y);
    for (int i = 0; i < n; ++i)
        if (a[i].y1 == a[i].y2) {
            if (a[i].y1 == y && a[i].x1 <= x && x <= a[i].x2) {
                printf("BORDER\n");
                return 0;
            }
        } else {
            if (a[i].x1 == x && a[i].y1 <= y && y <= a[i].y2) {
                printf("BORDER\n");
                return 0;
            }
            if (a[i].x1 < x && a[i].y1 <= y && y < a[i].y2) in = !in;
        }
    if (in) printf("INSIDE\n");
    else printf("OUTSIDE\n");
    return 0;
}
