// Euler Tour
#include <cstdio>
#include <vector>
using namespace std;

vector<int> e, ans;
vector<vector<int>> out;

void put(int no) {
    for (int i: out[no]) if (e[i] != -1) {
        auto ei = e[i];
        e[i] = e[i ^ 1] = -1;
        put(ei);
        ans.emplace_back(i);
    }
}

int main() {
    int n;
    scanf("%d", &n);
    e.resize(n << 1);
    out.resize(7);
    for (int i = 0; i < n; ++i) {
        int s, t;
        scanf("%d%d", &s, &t);
        e[i + i] = t;
        out[s].emplace_back(i + i);
        e[i + i + 1] = s;
        out[t].emplace_back(i + i + 1);
    }
    vector<int> odd;
    for (int i = 0; i < 7; ++i)
        if (out[i].size() & 1) odd.emplace_back(i);
    if (odd.size() > 2) {
        printf("No solution\n");
        return 0;
    }
    put(odd.empty() ? e[0] : odd[0]);
    if ((int)ans.size() < n) {
        printf("No solution\n");
        return 0;
    }
    for (int i = 0; i < (int)ans.size(); ++i)
        printf("%d %c\n", (ans[i] >> 1) + 1, ans[i] & 1 ? '+' : '-');
    return 0;
}
